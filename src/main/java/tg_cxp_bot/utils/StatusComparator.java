package tg_cxp_bot.utils;

public final class StatusComparator {
	
	public static String compareStatus(int status1, int status2) {
		if (status1 == status2) {
			return null;
		}
		
		if ((status1 != -1) && (status2 != -1)) {
			if (status1 == status2) {
				return null;
			}
			char sign = (status1 < status2) ? '-' : '+';
			
			return "(" + sign + Integer.toString(Math.abs(status1-status2)) + ")"; 
		}
		return null;
	}
}
